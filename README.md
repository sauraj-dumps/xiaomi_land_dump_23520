## land-user 6.0.1 MMB29M V10.2.2.0.MALMIXM release-keys
- Manufacturer: xiaomi
- Platform: msm8937
- Codename: land
- Brand: Xiaomi
- Flavor: land-user
- Release Version: 6.0.1
- Id: MMB29M
- Incremental: V10.2.2.0.MALMIXM
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-GB
- Screen Density: undefined
- Fingerprint: Xiaomi/land/land:6.0.1/MMB29M/V10.2.2.0.MALMIXM:user/release-keys
- OTA version: 
- Branch: land-user-6.0.1-MMB29M-V10.2.2.0.MALMIXM-release-keys
- Repo: xiaomi_land_dump_23520


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
