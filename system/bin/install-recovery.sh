#!/system/bin/sh
if ! applypatch -c EMMC:/dev/block/bootdevice/by-name/recovery:22992206:3fb628cc1988e25019f8afd1ba2ebe108e38068d; then
  applypatch -b /system/etc/recovery-resource.dat EMMC:/dev/block/bootdevice/by-name/boot:21192010:4ee9420c741cab9d3ed32b8e65fcaff991060f94 EMMC:/dev/block/bootdevice/by-name/recovery 3fb628cc1988e25019f8afd1ba2ebe108e38068d 22992206 4ee9420c741cab9d3ed32b8e65fcaff991060f94:/system/recovery-from-boot.p && log -t recovery "Installing new recovery image: succeeded" || log -t recovery "Installing new recovery image: failed"
else
  log -t recovery "Recovery image already installed"
fi
